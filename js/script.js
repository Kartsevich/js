 var urlRu = "https://rago.herokuapp.com/api/words/ru",
     urlEn = "https://rago.herokuapp.com/api/words/en";

 var RuId = null;
 var EnId = null;

 var ActiveRow = null;

 function addList(value) {
     $('#select').append('<option value="' + value.id + ' ">' + value.word + '</option>');

 }


 //---------------------------------------------------//
 $(document).ready(function() {

 	 $('#set_words').click(function() { 
         post("#word", "#enWords");
        
     })

     $("#del_words").click(function() {

         deleteEnWord($("#select").val());
     })

     $("#okButton").click(function() {
         var enWordId = $("#select").val(),
             enWordText = $("#editEnWord").val(),
             arrEditRuWords = $("#editRuWords").val().split(/;| |,/),
             arrCurrRuWords = [],
             settings = {
                 "async": true,
                 "crossDomain": true,
                 "url": "",
                 "method": "GET",
                 "headers": {
                     "content-type": "application/json"
                 },
                 "processData": false,
                 "data": " "
             }

         deleteArrRuWords(enWordId);

         for (var i = arrEditRuWords.length - 1; i >= 0; i--) {
             if (arrEditRuWords[i] == "")
                 arrEditRuWords.splice(i, 1);
         }

         settings.url = urlRu;
         settings.method = 'GET';

         $.ajax(settings).done(function(response) { // get arr Ru words
             settings.method = "POST";
             var index;
             for (var i = response.length - 1; i >= 0; i--) {
                 index = -1;
                 if (response[i] == null)
                     break;
                 index = arrEditRuWords.indexOf(response[i].word)
                 if (index > -1) { //if index > -1 add existed RuWord
                     console.log("ADD OLD" + " " + response[i].word);
                     settings.url = "https://rago.herokuapp.com/api/words/en-ru?en=" + enWordId + "&ru=" + response[i].id;
                     $.ajax(settings);
                     arrEditRuWords.splice(index, 1);

                 }
             }

             $(arrEditRuWords).each(function(kay, value) { //  add New RuWord
                 console.log("ADD NEW" + " " + value);
                 settings.url = urlRu;
                 settings.method = 'POST';
                 settings.data = "{\n\t\"word\":\"" + value + "\"\n}";
                 $.ajax(settings).done(function(response) {
                     settings.url = "https://rago.herokuapp.com/api/words/en-ru?en=" + enWordId + "&ru=" + response.id;
                     settings.data = "";
                     $.ajax(settings).done(updateTable());
                 })
             })
         })

         settings.url = urlEn;
         settings.method = 'POST';
         settings.data = "{\n\t\"id\" : " + enWordId + ",\n\t\"word\":\"" + enWordText + "\"\n}";
         $.ajax(settings);
     });

     $("#select").bind("change", function() {

         var settings = {
             "async": true,
             "crossDomain": true,
             "url": urlEn + "/" + $("#select").val(),
             "method": "GET"
         }

         $.ajax(settings).done(function(response) {

             $('#editEnWord').val(response.word);
             $('#editRuWords').val('');

             $.each(response.ruWords, function(key, value) {
                 $('#editRuWords').val() == null ?
                     $('#editRuWords').val(value.word + "; ") :
                     $('#editRuWords').val($('#editRuWords').val() + value.word + "; ");
             });
         });
     });

     updateTable(); 
 });
 //---------------------///
 function updateTable() {
     setTimeout(function() {
         console.log("update");
         $('#select').find('option').remove();
         $("#myTable").find("tr:gt(0)").remove();
         $.ajax({
             dataType: "json",
             url: urlEn,
             method: "GET",
             success: function(data) {
                 $(data).each(function(key, value) {
                     console.log(value);
                     addRow('myTable', value);
                     addList(value);
                 })

             }
         })
     }, 300);

 }

 //-----------------//
 function postEnRu(idRu, idEn) {
     console.log("Ru Id -" + idRu + "En Id" + idEn);
     var settings = {
         "async": true,
         "crossDomain": true,
         "url": "https://rago.herokuapp.com/api/words/en-ru?en=" + idEn + "&ru=" + idRu,
         "method": "POST",
         "headers": {
             "cache-control": "no-cache",
             "postman-token": "586f548c-c6a7-88b8-f851-de3f104853cf"
         }
     }
     $.ajax(settings)
 }

 //-----------------//

 function deleteEnWord(id) {
     var settings = {
         "async": true,
         "crossDomain": true,
         "url": "",
         "method": "GET",
         "headers": {
             "content-type": "application/json",
         }
     }
     settings.url = urlEn + "/" + id;
     $.ajax(settings).done(function(response) { //----delite en-ru 
         settings.method = "DELETE";
         var countRuWord = response.ruWords.length;
         if (countRuWord > 0) {
             $(response.ruWords).each(function(key, value) {
                 console.log(key);
                 settings.url = "https://rago.herokuapp.com/api/words/en-ru?en=" + response.id + "&ru=" + value.id;
                 $.ajax(settings).done(function(response) {
                     if (countRuWord - key <= 1) {
                         settings.method = "DELETE";
                         settings.url = urlEn + "/" + id;
                         $.ajax(settings).done(updateTable());
                     }
                 });
             })
         } else {
             settings.method = "DELETE";
             settings.url = urlEn + "/" + id;
             $.ajax(settings).done(updateTable());
         }
     })
 }
 //-----------------//
 function deleteArrRuWords(id) {

     var settings = {
         "async": true,
         "crossDomain": true,
         "url": "",
         "method": "GET",
         "headers": {
             "content-type": "application/json",
         }
     }

     settings.url = urlEn + "/" + id;

     $.ajax(settings).done(function(response) { //----delite en-ru 
         settings.method = "DELETE";
         $(response.ruWords).each(function(key, value) {
             settings.url = "https://rago.herokuapp.com/api/words/en-ru?en=" + response.id + "&ru=" + value.id;
             $.ajax(settings);
         })
     })
 }
 //-----------------//
 function existWord(colum, word) {
     var exist = false;
     var list = document.getElementById("myTable").getElementsByTagName("tr");
     for (var i = list.length - 1; i >= 1; i--) {
         if (word == list[i].getElementsByTagName("td")[colum].innerHTML)
             exist = true;
     }
     return exist;
 }
 //-----------------//

 function post(idWord, idEnWord) {
 	console.log("SDSDSDS");

     var numColumRuWord = 1,
         numColumEnWord = 0;

         console.log($(idWord).val() + "  - " + $(idEnWord).val());

     if ($(idWord).val() == "" || $(idEnWord).val() == "") {
         return;
     }

     if (existWord(numColumEnWord, idEnWord)) { console.log("EXIST EN"); return; }

     var settings = {
         "async": true,
         "crossDomain": true,
         "url": "",
         "method": "POST",
         "headers": {
             "content-type": "application/json"
         },
         "processData": false,
         "data": " "
     }

     settings.url = urlRu;
     settings.data = "{\"word\":\"" + $(idEnWord).val() + "\" }"

     $.ajax(settings).done(
         function(response) {
             RuId = response.id;
             if (RuId != null && EnId != null) {
                 postEnRu(RuId, EnId);
                 RuId = null;
                 EnId = null;
             }
         }
     )

     settings.url = urlEn;
     settings.data = "{\"word\":\"" + $(idWord).val() + "\" }"

     $.ajax(settings).done(
         function(response) {
             EnId = response.id;
             if (RuId != null && EnId != null) {
                 postEnRu(RuId, EnId);
                 RuId = null;
                 EnId = null;
                 updateTable();
             }
         }
     )
 }
 //-----------------//
 function addRow(nameTable, data) {
     var tbody = document.getElementById(nameTable).getElementsByTagName("tbody")[0];
     var row = document.createElement("tr")

     var exist = false

     var word = document.createElement("td")
     var ruWords = document.createElement("td")

     var list = document.getElementById('myTable'),
         rows = list.getElementsByTagName("tr")

     row.id = "rows-" + rows.length;
     row.className = "rows";


     word.id = data.id;
     word.appendChild(document.createTextNode(data.word))
     $.each(data.ruWords, function(key, value) {
         ruWords.appendChild(document.createTextNode(value.word + "; "))
         if (ruWords.id == '')
             ruWords.id = value.id
         else
             ruWords.id = ruWords.id + " " + value.id
     })


     row.appendChild(word);
     row.appendChild(ruWords);
     tbody.appendChild(row);


 }