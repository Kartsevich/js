 var urlRu = "https://rago.herokuapp.com/api/words/ru",
        urlEn = "https://rago.herokuapp.com/api/words/en";

    var RuId = null;
    var EnId = null;

    var ActiveRow = null;
    //---------------------------------------------------//
    function updateTable() {
        console.log("update");
        var list = document.getElementById("myTable").getElementsByTagName("tr"); 
        // var rows = list.getElementsByTagName("tr")

        // for (var i=1; i< rows.length; ++i) {
        //  $("#"+  rows[i].id).remove();
        //  }  

        $.ajax({
            dataType: "json",
            url: urlEn,
            method: "GET",
            success: function(data) {
                $(data).each(function(key, value) {
                    addRow('myTable', value);
                })

            }
        })
    }
    //---------------------------------------------------//

    $(document).ready(function() {


        updateTable()

        $('#set_words').click(function() {
            post("#word", "#enWords")
        })
    });
    //---------------------------------------------------//
    function postEnRu(idRu, idEn) {
        console.log("Ru Id -" + idRu + "En Id" + idEn);
        var settings = {
            "async": true,
            "crossDomain": true,
            "url": "https://rago.herokuapp.com/api/words/en-ru?en=" + idEn + "&ru=" + idRu,
            "method": "POST",
            "headers": {
                "cache-control": "no-cache",
                "postman-token": "586f548c-c6a7-88b8-f851-de3f104853cf"
            }
        }

        $.ajax(settings).done(function(response) {
            console.log("обновление таблицы");
            updateTable()
        });
    }

    //---------------------------------------------------//
    function delWords(url, id) {
        var settings = {
            "async": true,
            "crossDomain": true,
            "url": "" + url + id,
            "method": "DELETE",
            "headers": {
                "content-type": "application/json",
            }
        }

        $.ajax(settings).done(function(response) {
            console.log(response);
        });
    }
    //---------------------------------------------------//
    function existWord(colum, word) {
        var exist = false;
        var list = document.getElementById("myTable").getElementsByTagName("tr");
        for (var i = list.length - 1; i >= 1; i--) {
            if (word == list[i].getElementsByTagName("td")[colum].innerHTML)
                exist = true;
        }
        return exist;
    }
    //---------------------------------------------------//
    function removePost(id) { 
        var settings = {
            "async": true,
            "crossDomain": true,
            "url": " ",
            "method": "DELETE"
        }

        var removeRow = document.getElementById(id);
        var listTd = removeRow.getElementsByTagName("td");
        var arrRuWords = listTd[1].id.split(' ');
        var enWord = listTd[0].id;

        $.when(function() {
            for (var i = arrRuWords.length - 1; i >= 0; i--) {
                settings.url = "https://rago.herokuapp.com/api/words/en-ru?en=" + enWord + "&ru=" + arrRuWords[i]
                $.ajax(settings).done(function(response) {
                    console.log(" del en-ru " + response.status);
                });
            }
            return true;
        }).then(function() {
            for (var i = arrRuWords.length - 1; i >= 0; i--) {
                settings.url = "https://rago.herokuapp.com/api/words/ru/" + arrRuWords[i]
                $.ajax(settings).done(function(response) {
                    console.log(" del ru " + response.status);
                    $("#"+id).remove()
                    updateTable()
                });

                if (i <= 0) {
                    settings.url = "https://rago.herokuapp.com/api/words/en/" + enWord
                    $.ajax(settings).done(function(response) {
                        console.log(" del en " + response.status);
                        $("#"+id).remove()
                        updateTable()
                    });
                }
            }
        })

        return true;
    }
    //---------------------------------------------------//

    function post(idWord, idEnWord) {

        var numColumRuWord = 1,
            numColumEnWord = 0;

        if ($(idWord).val() == "" || $(idEnWord).val() == "") {
            return;
        }

        if (existWord(numColumEnWord, idEnWord)) { console.log("EXIST EN"); return; }

        var settings = {
            "async": true,
            "crossDomain": true,
            "url": "",
            "method": "POST",
            "headers": {
                "content-type": "application/json"
            },
            "processData": false,
            "data": " "
        }

        settings.url = urlRu;
        settings.data = "{\"word\":\"" + $(idEnWord).val() + "\" }"

        $.ajax(settings).done(
            function(response) {
                RuId = response.id;
                if (RuId != null && EnId != null) {
                    postEnRu(RuId, EnId);
                    RuId = null;
                    EnId = null;
                }
            }
        )

        settings.url = urlEn;
        settings.data = "{\"word\":\"" + $(idWord).val() + "\" }"

        $.ajax(settings).done(
            function(response) {
                EnId = response.id;
                if (RuId != null && EnId != null) {
                    postEnRu(RuId, EnId);
                    RuId = null;
                    EnId = null;
                }
            }
        )
    }
    //---------------------------------------------------//
    function addRow(nameTable, data) {
        var tbody = document.getElementById(nameTable).getElementsByTagName("tbody")[0];
        var row = document.createElement("tr")

        var exist = false

        var word = document.createElement("td")
        var ruWords = document.createElement("td")

        var list = document.getElementById('myTable'),
            rows = list.getElementsByTagName("tr")

        row.id = "rows-" + rows.length;
        row.className = "rows";

        for (var i = 1; i < rows.length; ++i) {
            if (rows[i].getElementsByTagName("td")[0].innerHTML == data.word) {
                exist = true;
                break;
            }

        };

        if (!exist) {
            word.id = data.id;
            word.appendChild(document.createTextNode(data.word))
            $.each(data.ruWords, function(key, value) {
                ruWords.appendChild(document.createTextNode(value.word + "; "))
                if (ruWords.id == '')
                    ruWords.id = value.id
                else
                    ruWords.id = ruWords.id + " " + value.id
            })


            row.appendChild(word);
            row.appendChild(ruWords);
            tbody.appendChild(row);

        }

        $("#" + row.id).click(function(pos) {
            $("#floatingmes").show();
            ActiveRow = row.id;
            $("#floatingmes").css('left', (pos.pageX + 3) + 'px').css('top', (pos.pageY) + 'px');

        })
        $("#" + row.id).mouseleave(function() {
            if (!($("#floatingmes").is(':hover'))) {
                $("#floatingmes").hide();
            }
        })
    }
    //---------------------------------------------------//

    $("#removeButton").click(function() {
        $("#floatingmes").hide();
        $.when(removePost(ActiveRow)).then(updateTable);

    })

    $("#editButton").click(function(pos) {
        $("#floatingmes").hide();
        $("#edit").css('left', (pos.pageX + 3) + 'px').css('top', (pos.pageY) + 'px');
        $("#edit").show();


        var currRow = document.getElementById(ActiveRow).getElementsByTagName("td")
        var oldEnWord = currRow[0].id
        var olRuWords = currRow[1].id.split(' ');

        $("#okButton").click(function() {
            removePost(ActiveRow)
            post("#editEnWord", "#editRuWords")
            $("#edit").hide();

        })

        $("#cancelButton").click(function() {
            $("#edit").hide();
            $("#editEnWord").val = ""
            $("#editRuWords").val = ""


        })

    })